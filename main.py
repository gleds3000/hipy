#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify, request

app = Flask(__name__)

characters = [
  { 'name': 'Gledson', 'Atividade': 'Devops', 'age': '33' },
  { 'name': 'VInicius', 'Atividade': 'dev', 'age': '25' },
  { 'name': 'Thiago', 'Atividade': 'Coringa', 'age': '30' },
  { 'name': 'Ferlin', 'Atividade': 'Gestor', 'age': '37' },
  { 'name': 'Paulo', 'Atividade': 'Consultor', 'age': '36' },
]


@app.route('/')
def get_characters():
  return jsonify(characters), 200

@app.route("/salvador")
def salvador():
    return "Hello, Salvador"

@app.route("/ferias")
def ferias():
    return "ferias em Julho 2019"

@app.route("/sobre")
def sobre():
    return "pagina sobre meu perfil"    

@app.route('/characters', methods=['POST'])
def add_characters():
  print(request.get_json())
  characters.append(request.get_json())
  return '', 204
  
@app.route('/characters/<string:name>', methods=['GET'])
def pesquisa(name):
    busca = characters[0]
    for i,c in enumerate(characters):
      if c['name'] == name:
        busca = characters[i]
    return jsonify({'characters' : busca})


@app.route('/characters/<string:name>',  methods=['DELETE'])
def del_characters(name):
  for i,c in enumerate(characters):
    if c['name'] == name:
      del characters[i]  
  return "vc excluiu " + characters[i], 204

@app.route('/characters/<string:name>', methods=['PUT'])
def upone(name):
    editar = request.get_json()
    for i,c in enumerate(qcharactersuarks):
      if c['name'] == name:
        characters[i] = editar  
    return "vc editou " + characters[i], 204


@app.route('/status')
def status():
  status = {'status' : 'up'}
  return jsonify(status), 200

if __name__ == '__main__':
  #app.run(debug=True, host='localhost')
  app.run(host='0.0.0.0', port=80)