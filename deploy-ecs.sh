#!/bin/bash

set -e

while getopts b:i:e:s: option
do
    case "${option}"
    in
        b) BASE_NAME=${OPTARG};;
        i) IMG_VERSION=${OPTARG};;
        e) DEPLOY_ENV=${OPTARG};;
        s) SERVICE_ID=${OPTARG};;
    esac
done

echo "BASE_NAME: " $BASE_NAME
echo "IMG_VERSION: " $IMG_VERSION
echo "DEPLOY_ENV: " $DEPLOY_ENV
echo "SERVICE_ID: " $SERVICE_ID
echo "SERVICE_NAME:" ${BASE_NAME}-${DEPLOY_ENV}-${SERVICE_ID}-service
if [ -z "$BASE_NAME" ]; then
    echo "exit: parametro BASE_NAME nao encontrado"
    exit;
fi

if [ -z "$SERVICE_ID" ]; then
    echo "exit: parametro SERVICE_ID nao encontrado"
    exit;
fi

if [ -z "$DEPLOY_ENV" ]; then
    echo "exit: parametro DEPLOY_ENV nao encontrado"
    exit;
fi

if [ -z "$IMG_VERSION" ]; then
    echo "exit: parametro IMG_VERSION nao encontrado"
    exit;
fi
#  hipy-Test-cd
TASK_FAMILY=family #${BASE_NAME}-${DEPLOY_ENV}-${SERVICE_ID}
SERVICE_NAME=${BASE_NAME}-${DEPLOY_ENV}-${SERVICE_ID}-service
CLUSTER_NAME=py-cluster

#IMAGE_PACEHOLDER="<IMAGE_VERSION>"

#CONTAINER_DEFINITION_FILE=$(cat cd.json)
#CONTAINER_DEFINITION="${CONTAINER_DEFINITION_FILE//$IMAGE_PACEHOLDER/$IMG_VERSION}"

#
#export TASK_VERSION=$(aws ecs register-task-definition \
#                    --family ${TASK_FAMILY} \
#                    --container-definitions "$CONTAINER_DEFINITION"  | jq --raw-output '.taskDefinition.revision')

export TASK_VERSION=$(aws ecs register-task-definition \
			--memory 2048 \
 			--cpu 512 \
			--cli-input-json file://cd.json  | jq --raw-output '.taskDefinition.revision')

echo "ECS Task Definition Atualizada: " $TASK_VERSION

#OLD_TASK_ID=$(aws ecs list-tasks --cluster $CLUSTER_NAME --desired-status RUNNING --family 
#$TASK_FAMILY:$TASK_VERSION | egrep "task/" | sed -E "s/.*task\/(.*)\"/\1/")
#INTERROMPER_TASK_ANTIGA=$(aws ecs stop-task --cluster $CLUSTER_NAME --task ${OLD_TASK_ID})
#echo "TASK ANTIGA:  $INTERROMPER_TASK_ANTIGA INTERROMPIDA"

if [ -n "$TASK_VERSION" ]; then
    echo "Atualizaçao do ECS Cluster: " $CLUSTER_NAME
    echo "Nome do Serviço: " $SERVICE_NAME
    echo "Nome Task Definition: " $TASK_FAMILY:$TASK_VERSION
    
    DEPLOYED_SERVICE=$(aws ecs update-service \
                        --cluster $CLUSTER_NAME \
                        --service $SERVICE_NAME \
                        --task-definition $TASK_FAMILY:$TASK_VERSION \
                        --force-new-deployment | jq --raw-output '.service.serviceName')
    echo "$DEPLOYED_SERVICE foi Atualizado com sucesso"
    
    #Envia email de sucesso #
    
    aws ecs list-tasks --cluster $CLUSTER_NAME --desired-status RUNNING
    
else
    echo "exit: Nenhuma task defition foi encontrada"
    #envia email de falha 
     
    exit;
fi