
FROM python:2.7-slim
#FROM python:2.7
#FROM alpine:3.1
#RUN apk add --update python py-pip


#RUN pip install Flask

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
COPY main.py /code/main.py
RUN pip install -r requirements.txt
#RUN ls -la
EXPOSE 80

ENV NAME desafio1

CMD ["python", "/code/main.py", "-p 80"]